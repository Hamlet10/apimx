# imagen base

FROM node:latest

# Directorio de la app en el contenedor
WORKDIR /app

# copiando archivos
ADD . /app

#Dependencias
RUN npm install

# puerto

EXPOSE 3000

#comando para ejecutar el servicio
CMD ["npm","start"]
